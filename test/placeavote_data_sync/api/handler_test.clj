(ns placeavote_data_sync.api.handler-test
  (:require [ring.mock.request :as mock]
            [placeavote_data_sync.api.handler :as handler]
            [placeavote_data_sync.bill.bill-dynamo-service :as bill-service]
            [placeavote_data_sync.bill.bill-dynamo-dao :as bill-dao]
            [placeavote_data_sync.bill.bill-parser :as bill-parser]
            [placeavote_data_sync.vote.vote-dynamo-service :as vote-service]
            [placeavote_data_sync.vote.vote-dynamo-dao :as vote-dao]
            [placeavote_data_sync.vote.vote-parser :as vote-parser]
            [placeavote_data_sync.people.people-csv-parser :as people-parser]
            [placeavote_data_sync.people.people-dynamo-dao :as people-dao]
            [placeavote_data_sync.vote.user-vote-builder :as user-vote-builder]
            [placeavote_data_sync.vote.user-vote-dynamo-dao :as user-vote-dao]
            [clojure.java.io :as io]
            [cheshire.core :as cheshire])
  (:use midje.sweet))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(def house-bill
  (bill-parser/parse-bill->map (retrieve-resource "h2317.xml")))
(def house-vote-bill
  (bill-parser/parse-bill->map (retrieve-resource "h10.xml")))
(def vote (vote-parser/parse-vote->map "resources/vote-test/votebill.xml"))
(def senate-bill
  (bill-parser/parse-bill->map (retrieve-resource "sr308.xml")))
(def people (people-parser/parse-csv->map "resources/people/legislators-current.csv"))
(def user-vote (user-vote-builder/build-user-vote "johnny" "aye" "h10"))

(against-background [(before :facts (do
                                        (try (bill-dao/delete-bills-table) (catch Exception e (str "no table to delete")))
                                        (bill-dao/create-bills-table)
                                        (try (bill-dao/delete-bill-subject-table) (catch Exception e (str "no table to delete")))
                                        (bill-dao/create-bill-subject-table)
                                        (try (vote-dao/delete-vote-table) (catch Exception e (str "no table to delete")))
                                        (vote-dao/create-vote-table)
                                        (try (vote-dao/delete-category-vote-table) (catch Exception e (str "no table to delete")))
                                        (vote-dao/create-category-vote-table)
                                        (try (vote-dao/create-voter-vote-table) (catch Exception e (str "no table to delete")))
                                        (vote-dao/create-voter-vote-table)
                                        (try (vote-dao/delete-bill-voter-table) (catch Exception e (str "no table to delete")))
                                        (vote-dao/create-bill-voter-table)
                                        (try (people-dao/delete-people-table) (catch Exception e (str "no table to delete")))
                                        (people-dao/create-people-table)
                                        (try (user-vote-dao/delete-user-vote-table) (catch Exception e (str "no table to delete")))
                                        (user-vote-dao/create-user-vote-table)
                                        (try (user-vote-dao/delete-bill-user-vote-table) (catch Exception e (str "no table to delete")))
                                        (user-vote-dao/create-bill-user-vote-table)))]

(fact "Retrieve bill by id"
  (bill-service/save-bills-and-subjects [house-bill])
  (let [response (handler/app (mock/request :get (str "/bill/" (:id house-bill))))]
    (:status response) => 200
    (:body response) => (cheshire/generate-string house-bill)))

(fact "Retrieve bills by state"
  (bill-service/save-bills-and-subjects [house-bill senate-bill])
  (let [referred (handler/app (mock/request :get (str "/bill/state/" (:state house-bill))))
        passed (handler/app (mock/request :get (str "/bill/state/" (:state senate-bill))))]
    (:status referred) => 200
    (:status passed) => 200
    (:body referred) => (cheshire/generate-string [house-bill])
    (:body passed) => (cheshire/generate-string [senate-bill])))

(future-fact "Retrieve bills by subject"
  (bill-service/save-bills-and-subjects [house-bill senate-bill])
  (let [res1 (handler/app (mock/request :get (str "/bill/subject/" (first (:subjects house-bill)))))
        res2 (handler/app (mock/request :get (str "/bill/subject/" (first (:subjects senate-bill)))))]
    (:status res1) => 200
    (:status res2) => 200
    (:body res1) => (cheshire/generate-string [house-bill])
    (:body res2) => (cheshire/generate-string [senate-bill])))

(fact "Retrieve bills by type"
  (bill-service/save-bills-and-subjects [house-bill senate-bill])
  (let [house (handler/app (mock/request :get (str "/bill/type/" (:type house-bill))))
        senate (handler/app (mock/request :get (str "/bill/type/" (:type senate-bill))))]
    (:status house) => 200
    (:status senate) => 200
    (:body house) => (cheshire/generate-string [house-bill])
    (:body senate) => (cheshire/generate-string [senate-bill])))

(fact "Retrieve votes by bill"
  (bill-service/save-bills-and-subjects [house-vote-bill])
  (vote-service/save-votes [vote])
  (let [voteResponse (handler/app (mock/request :get (str "/vote/" (:vote house-vote-bill))))]
    (:status voteResponse) => 200
    (:body voteResponse) => (cheshire/generate-string vote)))

(fact "Retrieve bills by sponsor"
  (bill-service/save-bills-and-subjects [house-bill])
  (let [bill (handler/app (mock/request :get (str "/bill/sponsor/" (:sponsor house-bill))))]
    (:status bill) => 200
    (:body bill) => (cheshire/generate-string [house-bill])))

(fact "Retrieve person by govtrack id"
  (people-dao/save-people people)
  (let [person (handler/app (mock/request :get (str "/people/" (:govtrack_id (first people)))))]
    (:status person) => 200
    (:body person) => (cheshire/generate-string (first people))))

(fact "Retrieve person by state"
  (people-dao/save-people people)
  (let [person (handler/app (mock/request :get (str "/people/state/" (:state (first people)))))]
    (:status person) => 200
    (:body person) => (cheshire/generate-string [(first people)])))

(fact "Retrieve person by party"
  (people-dao/save-people people)
  (let [person (handler/app (mock/request :get (str "/people/party/" (:party (first people)))))]
    (:status person) => 200
    (count people) => (count (cheshire/parse-string (:body person)))))

(fact "Register user vote for bill"
  (let [response (handler/app (mock/request :put "/user/vote" "{\"username\":\"johnny\",\"bill\":\"h10\",\"vote\":\"aye\"}"))
        user-vote (handler/app (mock/request :get "/user/vote/johnny"))
        bill-user-votes (handler/app (mock/request :get "/user/vote/johnny/h10"))
        bill-votes (handler/app (mock/request :get "/bill/vote/h10"))]
   (:status response) => 201
   (:status user-vote) => 200
   (:status bill-user-votes) => 200
   (:status bill-votes) => 200))
)
