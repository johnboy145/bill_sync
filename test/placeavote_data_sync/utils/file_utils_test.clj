(ns placeavote_data_sync.utils.file-utils-test
  (:use midje.sweet)
  (:require [placeavote_data_sync.utils.file-utils :refer :all]))


(fact "Retrieve map of bill root level attributes given file location
      and a vector is attributes"
      (retrieve-root-attributes "h2317.xml" [:session :type :number :updated]) =>
             (contains
                {:session "113"
                 :type "h"
                 :number "2317"
                 :updated "2014-07-09T04:51:32-04:00"}))

(fact "Retrieve value of given file and tag"
      (retrieve-tag-value "h2317.xml" :state) => "REFERRED")

(fact "Retrieve attribute value of given file, tag and attribute"
      (retrieve-tag-attr-value "h2317.xml" :sponsor :id) => "400230")



