(ns placeavote_data_sync.dynamo.dynamo-dao-test
  (:require [placeavote_data_sync.bill.bill-parser :as parser]
            [placeavote_data_sync.dynamo.dynamo-dao :as dao]
            [clojure.java.io :as io])
  (:use [midje.sweet]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(def house-bill
  (parser/parse-bill->map (retrieve-resource "h2317.xml")))

(fact "Split a collection of 50 items into 2 seperate collections 25 items"
  (let [collectionOfTwo (dao/split-dynamo-collection (repeat 25 (conj [] house-bill)))]
    (count collectionOfTwo) => 1))

(fact "Split a collection of 30 items into 2 seperate collections one with 25 items and one with 10"
  (let [collectionOfTwo (dao/split-dynamo-collection (repeat 60 (conj [] house-bill)))]
    (count collectionOfTwo) => 3
    (count (first collectionOfTwo)) => 25
    (count (nth collectionOfTwo 1)) => 25
    (count (nth collectionOfTwo 2)) => 10))
