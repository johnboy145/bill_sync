(ns placeavote_data_sync.vote.user-vote-dynamo-dao-test
  (:require [placeavote_data_sync.vote.user-vote-dynamo-dao :as dao]
            [placeavote_data_sync.vote.user-vote-builder :as builder])
  (:use [midje.sweet]))

(def vote (builder/build-user-vote "johnny5" "aye" "h10"))
(def vote2 (builder/build-user-vote "johnny5" "aye" "h101"))

(against-background [(before :facts (do
                                        (try (dao/delete-user-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-user-vote-table)
                                        (try (dao/delete-bill-user-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-bill-user-vote-table)))]


(fact "Given a user vote, persist to dynamo and retrieve by username and bill"
  (dao/save-user-vote vote)
  (let [user-vote (dao/get-user-vote-by-username-bill (:username vote) (:bill vote))]
    user-vote => (contains vote)))

(fact "Given two user votes, persist to dynamo and retrieve both by username"
  (dao/save-user-vote vote)
  (dao/save-user-vote vote2)
  (let [user-votes (dao/get-user-vote-by-username (:username vote))]
    (count user-votes) => 2))

(fact "Given a user vote, update vote with a yes vote"
  (dao/save-user-vote vote)
  (dao/update-bill-user-vote (:bill vote) :aye)
  (let [bill-user-vote (dao/get-bill-user-vote-by-bill (:bill vote))]
    bill-user-vote => (contains {:bill (:bill vote) :aye 1 :nay 0})))

(fact "Given a user vote, update vote with a no vote"
  (dao/save-user-vote vote)
  (dao/update-bill-user-vote (:bill vote) :nay)
  (let [bill-user-vote (dao/get-bill-user-vote-by-bill (:bill vote))]
    bill-user-vote => (contains {:bill (:bill vote) :aye 0 :nay 1})))

(fact "Given a user vote, and an existing bill vote record, then update vote with a yes vote"
  (dao/save-user-vote vote)
  (dao/update-bill-user-vote (:bill vote) :aye)
  (dao/update-bill-user-vote (:bill vote) :aye)
  (dao/update-bill-user-vote (:bill vote) :nay)
  (let [bill-user-vote (dao/get-bill-user-vote-by-bill (:bill vote))]
    bill-user-vote => (contains {:bill (:bill vote) :aye 2 :nay 1})))
)
