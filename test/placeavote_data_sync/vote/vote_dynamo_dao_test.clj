(ns placeavote_data_sync.vote.vote-dynamo-dao-test
    (:require [clojure.java.io :as io]
              [placeavote_data_sync.vote.vote-dynamo-dao :as dao]
              [placeavote_data_sync.vote.vote-parser :as parser])
    (:use [midje.sweet]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(def amendment (parser/parse-vote->map "resources/vote-test/ammendment.xml"))
(def amendment2 (parser/parse-vote->map "resources/vote-test/ammendment2.xml"))
(def nomination (parser/parse-vote->map "resources/vote-test/nomination.xml"))
(def amendment-category (parser/parse-category-vote->map amendment))
(def amendment-category2 (parser/parse-category-vote->map amendment2))
(def nomination-category (parser/parse-category-vote->map nomination))

(against-background [(before :facts (do
                                        (try (dao/delete-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-vote-table)
                                        (try (dao/delete-category-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-category-vote-table)
                                        (try (dao/create-voter-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-voter-vote-table)
                                        (try (dao/delete-bill-voter-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-bill-voter-table)))]

(fact "Persist Vote to dynamo and retrieve by id"
 (dao/save-vote amendment)
 (let [vote (dao/get-vote-by-id (:id amendment))]
   (:id amendment) => (:id vote)))

(fact "Persist Category Vote to dynamo and retrieve by category"
 (dao/save-category-vote amendment-category)
 (dao/save-category-vote amendment-category2)
 (let [votes (dao/get-category-vote-by-category (:category amendment-category))]
   (count votes) => 2))

(fact "Persist Category Votes to dynamo and retrieve by category"
 (dao/save-category-votes [amendment-category amendment-category2])
 (let [votes (dao/get-category-vote-by-category (:category amendment-category))]
   (count votes) => 2))

(fact "Persist Vote to dynamo and retrieve by chamber"
 (dao/save-vote amendment)
 (dao/save-vote nomination)
 (let [amendmentVotes (dao/get-vote-by-chamber (:where amendment))
       nominationVotes (dao/get-vote-by-chamber (:where nomination))]
   (count amendmentVotes) => 1
   (count nominationVotes) => 1))

(fact "Batch write Votes to dynamo and retrieve by chamber"
 (dao/save-votes [amendment nomination])
 (let [amendmentVotes (dao/get-vote-by-chamber (:where amendment))
       nominationVotes (dao/get-vote-by-chamber (:where nomination))]
   (count amendmentVotes) => 1
   (count nominationVotes) => 1))

 (fact "Batch write voters-votes to dynamo and retrieve by voter id"
  (dao/save-voter-votes amendment)
  (let [voter-vote (dao/get-voter-votes-by-id (:id (first (:voters amendment))))]
   (count voter-vote) => 1))

 (fact "Batch write voters-votes to dynamo and retrieve by voter id"
  (dao/save-voters-votes [amendment amendment2])
  (let [voter-vote (dao/get-voter-votes-by-id (:id (first (:voters amendment))))
        voter-vote2 (dao/get-voter-votes-by-id (:id (first (:voters amendment2))))]
   (count voter-vote) => 1
   (count voter-vote2) => 1))

 (fact "Batch write bill-voters to dynamo and retrieve by bill id"
  (dao/save-bill-voters [amendment amendment2])
  (let [voter-vote (dao/get-bill-voters-by-id (:bill (first (:voters amendment))))
        voter-vote2 (dao/get-bill-voters-by-id (:bill (first (:voters amendment2))))]
   (count voter-vote) => 6
   (count voter-vote2) => 4))

)
