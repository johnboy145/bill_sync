(ns placeavote_data_sync.vote.vote-parser-test
    (:use [midje.sweet])
    (:require [clojure.java.io :as io]
              [placeavote_data_sync.vote.vote-parser :as parser]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(fact "Parse root level vote attributes,
             e.g where, session, year, roll, source,
                 datetime updated aye, nay, nv and present and id"

  (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
    (contains {:where "house" :session "113" :year "2013"
               :roll "325" :source "house.gov"
               :datetime "2013-07-09T23:01:00-04:00"
               :updated "2014-06-18T11:21:41-04:00"
               :aye "81" :nay "335" :nv "18" :present "0"
               :id "h325-2013"}))

(fact "Parse vote category tag"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:category "amendment"}))

(fact "Parse vote type tag"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:type "On the Amendment"}))

(fact "Parse vote question tag"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:question "On Agreeing to the Amendment: Amendment 21 to H R 2609"}))

(fact "Parse vote required tag"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:required "1/2"}))

(fact "Parse vote result tag"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:result "Failed"}))

(fact "Parse vote bill tag"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:bill {:id "h2609" :session "113"}}))

(fact "Don't parse vote bill for votes without a bill"
      (let [result (parser/parse-vote->map "resources/vote-test/nomination.xml")]
        (contains? result :bill) => false))

(fact "Parse vote voter tags"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:voters [{:id "412500", :state "NV", :value "Aye" :bill "h2609" :vote "h325-2013"}
                          {:id "400021", :state "CA", :value "Aye" :bill "h2609" :vote "h325-2013"}
                          {:id "400004", :state "AL", :value "No" :bill "h2609" :vote "h325-2013"}
                          {:id "400006", :state "LA", :value "No" :bill "h2609" :vote "h325-2013"}
                          {:id "412410", :state "FL", :value "Not Voting" :bill "h2609" :vote "h325-2013"}
                          {:id "400439", :state "FL", :value "Not Voting" :bill "h2609" :vote "h325-2013"}]}))

(fact "Parse vote amendment"
      (parser/parse-vote->map "resources/vote-test/ammendment.xml") =>
      (contains {:amendment {:ref "bill-serial" :session "113" :number "21"}}))

(fact "Don't parse vote amendment for votes without one"
      (let [result (parser/parse-vote->map "resources/vote-test/nomination.xml")]
        (contains? result :amendment) => false))

(fact "Given a vote, construct a category vote"
      (parser/parse-category-vote->map (parser/parse-vote->map "resources/vote-test/nomination.xml")) =>
      (contains {:category "nomination" :id "s264-2013"
                 :question "On the Nomination PN243: Patricia M. Wald, of the District of Columbia, to be a Member of the Privacy and Civil Liberties Oversight Board for a term expiring January 29, 2019"}))
