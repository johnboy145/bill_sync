(ns placeavote_data_sync.vote.user-vote-builder-test
  (:require [placeavote_data_sync.vote.user-vote-builder :as builder])
  (:use [midje.sweet]))

(fact "Given a username, vote and bill, then construct user vote map"
  (let [user-vote (builder/build-user-vote "johnny5" "aye" "h10")]
    user-vote => (contains {:username "johnny5" :vote "aye" :bill "h10"})
    (contains? user-vote :created) => true))
