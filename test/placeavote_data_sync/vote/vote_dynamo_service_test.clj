(ns placeavote_data_sync.vote.vote-dynamo-service-test
  (:use midje.sweet)
  (:require [placeavote_data_sync.vote.vote-dynamo-service :as service]
            [placeavote_data_sync.vote.vote-dynamo-dao :as dao]
            [placeavote_data_sync.vote.vote-parser :as parser]
            [clojure.java.io :as io]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(def amendment (parser/parse-vote->map "resources/vote-test/ammendment.xml"))
(def amendment2 (parser/parse-vote->map "resources/vote-test/ammendment2.xml"))
(def nomination (parser/parse-vote->map "resources/vote-test/nomination.xml"))

(against-background [(before :facts (do
                                        (try (dao/delete-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-vote-table)
                                        (try (dao/delete-category-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-category-vote-table)
                                        (try (dao/create-voter-vote-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-voter-vote-table)
                                        (try (dao/delete-bill-voter-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-bill-voter-table)))]


  (fact "Given a collection of two votes, then persist two vote records"
   (service/save-votes [amendment amendment2])
   (let [vote1 (service/get-vote-by-id (:id amendment))
         vote2 (service/get-vote-by-id (:id amendment2))]
     (:id vote1) => (:id amendment)
     (:id vote2) => (:id amendment2)))


  (fact "Given a collection of two votes, then persist voter votes records"
   (service/save-votes [amendment nomination])
   (let [voter1 (service/get-voter-votes-by-id (:id (first (:voters amendment))))
         voter2 (service/get-voter-votes-by-id (:id (first (:voters nomination))))]
     (:id (first voter1)) => (:id (first (:voters amendment)))
     (:id (first voter2)) => (:id (first (:voters nomination)))))


  (fact "Given a collection of two votes, then persist two votes and retrieve by category"
   (service/save-votes [amendment nomination])
   (let [vote1 (service/get-category-votes-by-category (:category amendment))
         vote2 (service/get-category-votes-by-category (:category nomination))]
     (:category (first vote1)) => (:category amendment)
     (:category (first vote2)) => (:category nomination)))

  (fact "Given a collection of two voters-votes, then persist two votes and retrieve by bill"
   (service/save-votes [amendment amendment2])
   (let [voter1 (service/get-bill-voters-by-bill (:bill (first (:voters amendment))))
         voter2 (service/get-bill-voters-by-bill (:bill (first (:voters amendment2))))]
     (:bill (first voter1)) => (:bill (first (:voters amendment)))
     (:bill (first voter2)) => (:bill (first (:voters amendment2)))))

)

