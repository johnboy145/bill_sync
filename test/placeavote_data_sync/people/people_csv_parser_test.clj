(ns placeavote_data_sync.people.people-csv-parser-test
  (:require [clojure.java.io :as io]
            [placeavote_data_sync.people.people-csv-parser :as parser])
  (:use [midje.sweet]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(fact "Given a csv file of people, parse each person into a map"
  (let [result (parser/parse-csv->map "resources/people/legislators-current.csv")]
    (count result) => 3
    result => (contains {:last_name "Brown"
                         :first_name "Sherrod"
                         :birthday "1952-11-09"
                         :gender "M"
                         :type "sen"
                         :state "OH"
                         :party "Democrat"
                         :url "http://www.brown.senate.gov"
                         :address "713 Hart Senate Office Building Washington DC 20510"
                         :phone "202-224-2315"
                         :contact_form "http://www.brown.senate.gov/contact"
                         :rss_url "http://www.brown.senate.gov/rss/feeds/?type=all&amp;"
                         :twitter "SenSherrodBrown"
                         :youtube "SherrodBrownOhio"
                         :youtube_id "UCgy8jfERh-t_ixkKKoCmglQ"
                         :bioguide_id "B000944"
                         :thomas_id "00136"
                         :opensecrets_id "N00003535"
                         :lis_id "S307"
                         :cspan_id "5051"
                         :govtrack_id "400050"
                         :votesmart_id "27018"
                         :ballotpedia_id "Sherrod Brown"
                         :washington_post_id "gIQA3O2w9O"
                         :icpsr_id "29389"
                         :wikipedia_id "Sherrod Brown"})))
