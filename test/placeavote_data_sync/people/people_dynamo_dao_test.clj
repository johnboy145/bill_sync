(ns placeavote_data_sync.people.people-dynamo-dao-test
  (:require [placeavote_data_sync.people.people-dynamo-dao :as dao]
            [placeavote_data_sync.people.people-csv-parser :as parser])
  (:use [midje.sweet]))

(def people (parser/parse-csv->map "resources/people/legislators-current.csv"))

(against-background [(before :facts (do
                                        (try (dao/delete-people-table) (catch Exception e (str "no table to delete")))
                                        (dao/create-people-table)))]

(fact "Given a collection of 3 people, persist to dynamo and retrieve by gov_id"
  (dao/save-people people)
  (let [person1 (dao/get-person-by-id (:govtrack_id (first people)))
        person2 (dao/get-person-by-id (:govtrack_id (nth people 1)))
        person3 (dao/get-person-by-id (:govtrack_id (nth people 2)))]
    person1 => (contains (first people))
    person2 => (contains (nth people 1))
    person3 => (contains (nth people 2))))

(fact "Given a collection of 3 people, persist to dynamo and retrieve by state"
  (dao/save-people people)
  (let [person1 (dao/get-person-by-state (:state (first people)))
        person2 (dao/get-person-by-state (:state (nth people 1)))
        person3 (dao/get-person-by-state (:state (nth people 2)))]
    (first person1) => (contains (first people))
    (first person2) => (contains (nth people 1))
    (first person3) => (contains (nth people 2))))

(fact "Given a collection of 3 people, persist to dynamo and retrieve by party"
  (dao/save-people people)
  (let [people (dao/get-person-by-party (:party (first people)))]
    (count people) => 3))

)
