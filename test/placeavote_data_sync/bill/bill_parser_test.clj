(ns placeavote_data_sync.bill.bill-parser-test
  (:use midje.sweet)
  (:require [placeavote_data_sync.bill.bill-parser :refer :all]
            [clojure.java.io :as io]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(fact "Parse a Bills Root level Attributes"
      (parse-bill->map (retrieve-resource "h2317.xml")) =>
             (contains
                {:session "113"
                 :type "h"
                 :number "2317"
                 :updated "2014-07-09T04:51:32-04:00"}))

(fact "Parse Bills current state"
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:state "REFERRED"}))

(fact "Parse Bills sponsor id"
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:sponsor "400230"}))

(fact "Parse Bills introduced date"
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:introduced "2013-06-11"}))

(fact "Parse Bill titles"
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:titles {:short "Counseling for Career Choice Act"
                                                           :official "To amend the Elementary and Secondary Education Act of 1965 to provide grants to States to establish a comprehensive school counseling program."}}))

(fact "Parse Bills cosponsor ids and join dates"
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:cosponsors ["412501" "400048"]}))

(fact "Parse Bills actions"
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:actions [{:datetime "2013-06-11" :state "REFERRED" :text "Referred to the House Committee on Education and the Workforce."}
                                                            {:datetime "2013-07-08" :text "Referred to the Subcommittee on Early Childhood, Elementary, and Secondary Education."}
                                                            {:datetime "2013-07-08" :text "Referred to the Subcommittee on Higher Education and Workforce Training."}]}))


(fact "Parse Bill committees"
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:committees [{:code "HSED" :name "House Education and the Workforce" :activity "Referral" }
                                                            {:code "HSED14" :name "House Education and the Workforce" :subcommittee "Early Childhood, Elementary, and Secondary Education" :activity "Referral"}
                                                            {:code "HSED13" :name "House Education and the Workforce" :subcommittee "Higher Education and Workforce Training" :activity "Referral"}]}))
(fact "Parse Bills Related Bills"
      (parse-bill->map (retrieve-resource "h2317.xml")) =>
        (contains {:relatedBills [{:relation "unknown" :session "113" :type "s" :number "282"}]}))

(fact "Parse bill subjects"
      (parse-bill->map (retrieve-resource "h2317.xml")) =>
      (contains {:subjects ["Education"
                            "Education programs funding"
                            "Elementary and secondary education"
                            "Employment and training programs"
                            "Higher education"
                            "Teaching, teachers, curricula"]}))


(fact "Parse Bills actions, when no actions exist, then do not include an actions attribute."
      (contains? (parse-bill->map (retrieve-resource "sr201.xml")) :actions) => false)

(fact "Parse Bill, ensure id attribute containing the type and number is created."
      (parse-bill->map (retrieve-resource "h2317.xml")) => (contains {:id "h2317"}))

(fact "Parse Bill Subjects, given Parsed Bill then create subject and id key mapping for subject term."
      (let [billSubjects (construct-bill-subject->map (parse-bill->map (retrieve-resource "h2317.xml")))]
        billSubjects => [{:subject "Education" :id "h2317" :title "Counseling for Career Choice Act" :updated "2014-07-09T04:51:32-04:00"}
                         {:subject "Education programs funding" :id "h2317" :title "Counseling for Career Choice Act" :updated "2014-07-09T04:51:32-04:00"}
                         {:subject "Elementary and secondary education" :id "h2317" :title "Counseling for Career Choice Act" :updated "2014-07-09T04:51:32-04:00"}
                         {:subject "Employment and training programs" :id "h2317" :title "Counseling for Career Choice Act" :updated "2014-07-09T04:51:32-04:00"}
                         {:subject "Higher education" :id "h2317" :title "Counseling for Career Choice Act" :updated "2014-07-09T04:51:32-04:00"}
                         {:subject "Teaching, teachers, curricula" :id "h2317" :title "Counseling for Career Choice Act" :updated "2014-07-09T04:51:32-04:00"}]))

(fact "Parse Bills actions, when no actions exist, then do not include an actions attribute."
      (contains? (first (construct-bill-subject->map (parse-bill->map (retrieve-resource "sr308.xml")))) :title) => false)

(fact "Parse bill vote tag"
      (parse-bill->map (retrieve-resource "h10.xml")) =>
      (contains {:vote "h217-2014"}))
