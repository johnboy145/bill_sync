(ns placeavote_data_sync.bill.bill-dynamo-dao-test
  (:require [placeavote_data_sync.bill.bill-dynamo-dao :as bill-dao]
            [placeavote_data_sync.bill.bill-parser :refer :all]
            [clojure.java.io :as io]
            [carica.core :refer :all])
  (:use [midje.sweet]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(def house-bill
  (parse-bill->map (retrieve-resource "h2317.xml")))
(def senate-bill
  (parse-bill->map (retrieve-resource "s1028.xml")))
(def senate2-bill
  (parse-bill->map (retrieve-resource "s1029.xml")))
(def resolution-bill
  (parse-bill->map (retrieve-resource "sr201.xml")))

(def house-bill-subjects
  (construct-bill-subject->map house-bill))

(with-redefs [config (override-config :dynamodb-creds {:access-key "<AWS_DYNAMODB_ACCESS_KEY>"
                                                       :secret-key "<AWS_DYNAMODB_SECRET_KEY>"
                                                       :endpoint "http://localhost:8000"})])
(against-background [(before :facts (do
                                        (try (bill-dao/delete-bills-table) (catch Exception e (str "no table to delete")))
                                        (bill-dao/create-bills-table)
                                        (try (bill-dao/delete-bill-subject-table) (catch Exception e (str "no table to delete")))
                                        (bill-dao/create-bill-subject-table)))]
  (fact "Persist A Bill, and retrieve by id"
      (bill-dao/save-bill house-bill)
      (let [bill (bill-dao/get-bill-by-id "h2317")]
        bill => (contains {:id "h2317"})
        bill => (contains {:type "h"})
        bill => (contains {:number "2317"})
        bill => (contains {:cosponsors ["412501" "400048"]})
        bill => (contains {:titles {:short "Counseling for Career Choice Act"
                                    :official "To amend the Elementary and Secondary Education Act of 1965 to provide grants to States to establish a comprehensive school counseling program."}}))
      (against-background (before :facts (do
                                           (try (bill-dao/delete-bills-table) (catch Exception e (str "no table to delete")))
                                           (bill-dao/create-bills-table)))))

(fact "Create Two Bills, one of type h and two of type s
                  When search bills by type s, Then return two Bills"
        (bill-dao/save-bill house-bill)
        (bill-dao/save-bill senate-bill)
        (bill-dao/save-bill senate2-bill)
        (let [byTypeResults (bill-dao/get-bills-by-type "s")]
          (count byTypeResults) => 2))

(fact "Create Two Bills,
      When querying bills by sponsor, Then return one bill."
        (bill-dao/save-bill house-bill)
        (bill-dao/save-bill senate-bill)
        (let [result (bill-dao/get-bills-by-sponsor (:sponsor house-bill))]
          (count result) => 1
          (:sponsor (first result)) => (:sponsor house-bill)))

(fact "Create Two Bills,
      When querying bills by state, Then return one REFERRED bill."
        (bill-dao/save-bill house-bill)
        (bill-dao/save-bill resolution-bill)
        (let [result (bill-dao/get-bills-by-state (:state house-bill))]
          (count result) => 1
          (:state (first result)) => (:state house-bill)))

(fact "Create Two Bills with batch write,
      When querying bills by state, Then return two REFERRED bills."
        (bill-dao/save-bills [house-bill senate-bill])
        (let [result (bill-dao/get-bills-by-state (:state house-bill))]
          (count result) => 2
          (:state (first result)) => (:state house-bill)))

(fact "Create A Bills and Parse Bill Subjects,
      When querying bills by subjects, Then return bills by subject type."
      (bill-dao/save-bill-subjects house-bill-subjects)
      (let [education (bill-dao/get-bills-by-subject "Education")
            Highereducation (bill-dao/get-bills-by-subject "Higher education")]
        (count education) => 1
        (:subject (first education)) => "Education"
        (count Highereducation) => 1
        (:subject (first Highereducation)) => "Higher education"))

)

