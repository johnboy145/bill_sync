(ns placeavote_data_sync.bill.bill-sync-test
  (:refer-clojure :exclude [name parents])
  (:use midje.sweet)
  (:require [placeavote_data_sync.utils.file-utils :refer :all]
            [placeavote_data_sync.bill.bill-parser :refer :all]
            [placeavote_data_sync.bill.bill-dynamo-service :as bill-dynamo-service]
            [placeavote_data_sync.bill.bill-dynamo-dao :as bill-dao]
            [placeavote_data_sync.bill.bill-sync :as bill-sync]
            [clojure.java.io :as io]
            [me.raynes.fs :refer :all]))


  (defn seed-test-data []
    (copy-dir (io/resource "bills/") "/tmp"))

  (against-background [(before :facts (do
                                        (seed-test-data)
                                        (try (bill-dao/delete-bills-table) (catch Exception e (str "no table to delete")))
                                        (bill-dao/create-bills-table)
                                        (try (bill-dao/delete-bill-subject-table) (catch Exception e (str "no table to delete")))
                                        (bill-dao/create-bill-subject-table)))]

    (fact "Given a bill type directory, then retrieve a list of bill directories"
      (let [billDirectories (list-directories "/tmp/bills/hr/")]
        (count billDirectories) => 2))

    (fact "Given a bill directory, then retrieve data.xml file"
      (let [bill (retrieve-file-from-dir "/tmp/bills/hr/hr123/data.xml")]
        (.getPath bill) => "/tmp/bills/hr/hr123/data.xml"))

    (fact "Given a directory of bills, then retrieve and parse all bills in each sub directory"
       (let [parsedBills (parse-bills-from-dir "/tmp/bills/hr/")]
         (count parsedBills) => 2))


    (fact "Given a Bill type directory, then recurively parse and persit bills to dynamo"
        (bill-sync/sync-bills-in-dir "/tmp/bills/hr/")
        (let [hr123 (bill-dynamo-service/get-bill-by-id "h123")
              hr1340 (bill-dynamo-service/get-bill-by-id "h1340")]
          (:id hr123) => "h123"
          (:id hr1340) => "h1340"))

    (fact "Given a Vector of Bill type directories, then recurively parse and persit bills to dynamo"
        (bill-sync/sync-bills-in-dirs ["/tmp/bills/hr/" "/tmp/bills/s/"])
        (let [hr123 (bill-dynamo-service/get-bill-by-id "h123")
              hr1340 (bill-dynamo-service/get-bill-by-id "h1340")
              s1172 (bill-dynamo-service/get-bill-by-id "s1172")
              s1173 (bill-dynamo-service/get-bill-by-id "s1173")]
          (:id hr123) => "h123"
          (:id hr1340) => "h1340"
          (:id s1172) => "s1172"
          (:id s1173) => "s1173"))

  )
