(ns placeavote_data_sync.bill.bill-dynamo-service-test
  (:use midje.sweet)
  (:require [placeavote_data_sync.bill.bill-dynamo-service :as bill-service]
            [placeavote_data_sync.bill.bill-dynamo-dao :as bill-dao]
            [placeavote_data_sync.bill.bill-parser :as bill-parser]
            [clojure.java.io :as io]))

(defn retrieve-resource [fileName]
  (-> fileName
      io/resource
      io/file))

(def house-bill
  (bill-parser/parse-bill->map (retrieve-resource "h2317.xml")))
(def senate-bill
  (bill-parser/parse-bill->map (retrieve-resource "sr308.xml")))


(defn get-bill-subjects [bill]
    (into []
      (for [subject (:subjects bill)]
        (let [bills (bill-service/get-bills-by-subject subject)]
          bills))))

(fact "Given a collection of two Bills, then persist 2 bills to Bill Table and 10 Bill Subjects"
  (bill-service/save-bills-and-subjects [house-bill senate-bill])
  (let [persisted-house-bill (bill-service/get-bill-by-id (:id house-bill))
        persisted-senate-bill (bill-service/get-bill-by-id (:id senate-bill))]
        (:id persisted-house-bill) => (:id house-bill)
        (:id persisted-senate-bill) => (:id senate-bill)
        (count (get-bill-subjects house-bill)) => 6)
 ;       (count (get-bill-subjects senate-bill)) => 4)
    (against-background (before :facts (do
                                         (try (bill-dao/delete-bills-table) (catch Exception e (str "no table to delete")))
                                         (bill-dao/create-bills-table)
                                         (try (bill-dao/delete-bill-subject-table) (catch Exception e (str "no table to delete")))
                                         (bill-dao/create-bill-subject-table)))))
