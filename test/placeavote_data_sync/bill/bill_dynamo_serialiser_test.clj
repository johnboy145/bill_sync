(ns placeavote_data_sync.bill.bill-dynamo-serialiser-test
  (:require [placeavote_data_sync.bill.bill-parser :refer :all]
            [taoensso.faraday :refer :all]
            [placeavote_data_sync.bill.bill-dynamo-serialiser :refer :all]
            [clojure.java.io :as io])
  (:use [midje.sweet]))

(def test-bill
  (parse-bill->map (-> "h2317.xml" io/resource io/file)))

(fact "Serialise bill titles map"
      (serialise-bill-map test-bill) =>
        (contains {:titles {:opts nil, :value {:map {:short "Counseling for Career Choice Act"
                                  :official "To amend the Elementary and Secondary Education Act of 1965 to provide grants to States to establish a comprehensive school counseling program."}}}}))


(fact "Serialise bill cosponsors" []
      (let [result (serialise-bill-map test-bill)]
        result => (contains {:introduced "2013-06-11" :cosponsors {:opts nil, :value {:vector ["412501" "400048"]}}})))
