(defproject placeavote-data-sync "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [midje "1.6.3"]
                 [org.clojure/data.zip "0.1.1"]
                 [cheshire "5.3.1"]
                 [com.taoensso/faraday "1.5.0"]
                 [com.taoensso/nippy "2.7.0"]
                 [me.raynes/fs "1.4.4"]
                 [sonian/carica "1.1.0"]
                 [compojure "1.2.0"]
                 [ring/ring-defaults "0.1.2"]
                 [javax.servlet/servlet-api "2.5"]
                 [ring-mock "0.1.5"]
                 [csv-map "0.1.1"]
                 [clj-time "0.8.0"]]
  :main placeavote_data_sync.sync.gov-sync
  :ring {:handler placeavote_data_sync.api.handler/app}
  :plugins [[lein-midje "3.1.3"]
            [lein-ring "0.8.13"]]
  :min-lein-version "2.0.0"
  :profiles {:production {:env {:production true}}})
