(ns placeavote_data_sync.dynamo.dynamo-dao
  (:require [taoensso.faraday :as far]
            [clojure.tools.logging :as log]))

(defn split-dynamo-collection [collection]
  (partition 25 25 nil collection))


(defn batch-write-item [creds table-name items]
  (doseq [part (split-dynamo-collection items)]
;      (Thread/sleep 500)
      (log/info (count part) " items being saved to dynamo table " table-name "!!!")
      (if (empty? part)
        (log/info "Empty partition")
        (far/batch-write-item creds
          {table-name
           {:put part}}))))

(defn save-item [creds table-name item]
  (log/info  " One items being saved to dynamo table " table-name "!!!")
  (far/put-item creds table-name item))
