(ns placeavote_data_sync.bill.bill-dynamo-dao

  (:require [taoensso.faraday :as far]
            [placeavote_data_sync.dynamo.dynamo-dao :as dynamo]
            [placeavote_data_sync.bill.bill-dynamo-serialiser :refer :all]
            [carica.core :refer :all]
            [clojure.tools.logging :as log]))

(def table-name "bills")
(def bill-subject-table-name "bill-subjects")

(def client-opts (config :dynamodb-creds))

(defn delete-bills-table []
  (far/delete-table client-opts table-name))

(defn delete-bill-subject-table []
  (far/delete-table client-opts bill-subject-table-name))

(defn create-bills-table []
  (far/ensure-table client-opts table-name [:id :s]
  {:throughput {:read 1 :write 1}
  :block? true
  :gsindexes [{:name "sponsor-idx"
               :hash-keydef [:sponsor :s]
               :range-keydef [:updated :s]
               :throughput {:read 2 :write 2}
               :projection :all}
              {:name "type-idx"
               :hash-keydef [:type :s]
               :range-keydef [:updated :s]
               :throughput {:read 2 :write 2}
               :projection :all}
              {:name "state-idx"
               :hash-keydef [:state :s]
               :range-keydef [:updated :s]
               :throughput {:read 2 :write 2}
               :projection :all}]}))

(defn create-bill-subject-table []
  (far/ensure-table client-opts bill-subject-table-name [:subject :s]
  {:throughput {:read 1 :write 1}
   :block? true
   :range-keydef [:id :s]}))



(defn save-bill [bill]
  "Persist Bill to DynamoDB"
  (far/put-item client-opts table-name (serialise-bill-map bill)))

(defn save-bills [bills]
  "Persist Bills to DynamoDB with batch write"
    (log/info (count bills) " bills saved to dynamo")
  (let [serialised-bills (serialise-bills bills)]
    (dynamo/batch-write-item client-opts table-name serialised-bills)))

(defn save-bill-subjects [billSubjects]
  "Persist Bill Subjects"
    (log/info (count billSubjects) " bill subjects saved to dynamo")
    (dynamo/batch-write-item client-opts bill-subject-table-name billSubjects))

(defn save-bill-subject [bill]
  "Persist Bill to DynamoDB"
  (dynamo/save-item client-opts bill-subject-table-name (serialise-bill-map bill)))

(defn get-bill-by-id [id]
  (deserialise-bill-map (far/get-item client-opts table-name {:id id})))

(defn get-bills-by-type [typ]
  (deserialise-bills-map (far/query client-opts table-name {:type [:eq typ]} {:index "type-idx"})))

(defn get-bills-by-sponsor [sponsor]
  (deserialise-bills-map (far/query client-opts table-name {:sponsor [:eq sponsor]} {:index "sponsor-idx"})))

(defn get-bills-by-state [state]
  (deserialise-bills-map (far/query client-opts table-name {:state [:eq state]} {:index "state-idx"})))

(defn get-bills-by-subject [subject]
  (far/query client-opts bill-subject-table-name {:subject [:eq subject]}))

