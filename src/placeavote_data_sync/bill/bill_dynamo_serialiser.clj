(ns placeavote_data_sync.bill.bill-dynamo-serialiser
    (:require [taoensso.faraday :refer :all]
              [taoensso.nippy :as nippy]))

(defmacro freeze-el [obj]
  (list 'cond
        (list '= (list 'class obj) clojure.lang.PersistentArrayMap) (list 'freeze {:map obj})
        (list '= (list 'class obj) clojure.lang.PersistentHashMap) (list 'freeze {:map obj})
        (list '= (list 'class obj) clojure.lang.PersistentVector) (list 'freeze {:vector obj})
        :else obj))

(defn unwrap-value [obj]
  (cond
   (contains? obj :vector) (:vector obj)
   (contains? obj :map) (:map obj)
   :else nil))

(defn unwrap-map [obj]
  (if (= (class obj) clojure.lang.PersistentArrayMap)
    (unwrap-value obj)
    obj))


(defn serialise-collection-for-dynamo [bill]
  (into bill
   (for [colKey (keys bill)]
    (let [frozenContent (freeze-el (colKey bill))]
    [colKey frozenContent]))))

(defn deserialise-bill-map [bill]
  (into bill
   (for [colKey (keys bill)]
     (let [unwrapped-map (unwrap-map (colKey bill))]
       {colKey unwrapped-map}))))

(defn deserialise-bills-map [bills]
  (for [bill bills]
    (into bill
     (for [colKey (keys bill)]
       (let [unwrapped-map (unwrap-map (colKey bill))]
         {colKey unwrapped-map})))))

(defn serialise-bill-map [bill]
   (serialise-collection-for-dynamo bill))

(defn serialise-bills [bills]
  (into []
    (for [bill bills]
      (serialise-collection-for-dynamo bill))))



