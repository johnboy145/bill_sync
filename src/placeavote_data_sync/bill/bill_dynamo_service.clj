(ns placeavote_data_sync.bill.bill-dynamo-service
  (:require [placeavote_data_sync.bill.bill-dynamo-dao :as bill-dao]
            [placeavote_data_sync.bill.bill-parser :as parser]))


(defn save-bills-subjects [bills]
  (doseq [bill bills]
    (bill-dao/save-bill-subjects (parser/construct-bill-subject->map bill))))

(defn save-bills-and-subjects [bills]
  (do
    (bill-dao/save-bills bills)
    (save-bills-subjects bills)))

(defn get-bill-by-id [id]
  (bill-dao/get-bill-by-id id))

(defn get-bills-by-state [state]
  (bill-dao/get-bills-by-state state))

(defn get-bills-by-type [ty]
  (bill-dao/get-bills-by-type ty))

(defn get-bills-by-subject [subject]
  (bill-dao/get-bills-by-subject subject))

(defn get-bills-by-sponsor [sponsor]
  (bill-dao/get-bills-by-sponsor sponsor))
