(ns placeavote_data_sync.bill.bill-parser
  (:require [placeavote_data_sync.utils.file-utils :refer :all]
            [clojure.xml :as xml]
            [clojure.java.io :as io]
            [clojure.zip :as zip]
            [clojure.data.zip.xml :as zip-xml]))

(defn- parse-tag [parsed-content tag]
    (if (empty? parsed-content)
      nil
      {tag parsed-content}))

(defn- parse-vote [root]
  (first (for [vote (zip-xml/xml-> root :actions :vote)]
    (str (zip-xml/attr vote :where) (zip-xml/attr vote :roll) "-" (subs (zip-xml/attr vote :datetime)  0 4)))))

(defn- parse-actions [root]
  (into []
        (into []
              (for [m (zip-xml/xml-> root :actions :action)]
                (-> (parse-tag (zip-xml/attr m :datetime) :datetime)
                    (conj (parse-tag (zip-xml/attr m :state) :state))
                    (conj (parse-tag (zip-xml/text m) :text)))))))

(defn- parse-cosponsors [root]
    (into []
          (for [m (zip-xml/xml-> root :cosponsors :cosponsor)]
            (zip-xml/attr m :id))))



(defn- parse-titles [root]
  (into {}
        (for [m (zip-xml/xml-> root :titles :title)]
          [(keyword (zip-xml/attr m :type))
           (zip-xml/text m)])))


(defn- parse-committees [root]
    (into []
          (into []
                (for [m (zip-xml/xml-> root :committees :committee)]
                  (-> (parse-tag (zip-xml/attr m :code) :code)
                      (conj (parse-tag (zip-xml/attr m :name) :name))
                      (conj (parse-tag (zip-xml/attr m :subcommittee) :subcommittee))
                      (conj (parse-tag (zip-xml/attr m :activity) :activity)))))))

(defn- parse-relatedBills [root]
    (into []
          (into []
                (for [m (zip-xml/xml-> root :relatedbills :bill)]
                  (-> (parse-tag (zip-xml/attr m :relation) :relation)
                      (conj (parse-tag (zip-xml/attr m :session) :session))
                      (conj (parse-tag (zip-xml/attr m :type) :type))
                      (conj (parse-tag (zip-xml/attr m :number) :number)))))))

(defn- parse-subjects [root]
    (into []
          (for [m (zip-xml/xml-> root :subjects :term)]
            (zip-xml/attr m :name))))

(defn- parse-bill-id [root]
  (let [number (zip-xml/attr root :number)
        typ (zip-xml/attr root :type)]
        (str typ number)))

(defn parse-bill->map [fileLoc]
  (let [root (-> fileLoc io/file xml/parse zip/xml-zip)]
    (->
     (retrieve-roots-attributes root [:session :type :number :updated])
     (conj (parse-tag (parse-bill-id root) :id))
     (conj (parse-tag (retrieve-tags-value root :state) :state))
     (conj (parse-tag (retrieve-tags-attr-value root :sponsor :id) :sponsor))
     (conj (parse-tag (retrieve-tags-attr-value root :introduced :datetime) :introduced))
     (conj (parse-tag (parse-titles root) :titles))
     (conj (parse-tag (parse-cosponsors root) :cosponsors))
     (conj (parse-tag (parse-actions root) :actions))
     (conj (parse-tag (parse-committees root) :committees))
     (conj (parse-tag (parse-relatedBills root) :relatedBills))
     (conj (parse-tag (parse-subjects root) :subjects))
     (conj (parse-tag (parse-vote root) :vote)))))


(defn construct-bill-subject->map [bill]
  (into []
        (for [subject (:subjects bill)]
          (-> {:subject subject}
              (conj (parse-tag (:id bill) :id))
              (conj (parse-tag (:short (:titles bill)) :title))
              (conj (parse-tag (:updated bill) :updated))))))

(defn parse-bills-from-dir [dir]
  (let [billDirs (list-directories dir)]
    (into []
      (for [dir billDirs]
        (parse-bill->map (str (.getPath dir) "/data.xml"))))))

