(ns placeavote_data_sync.bill.bill-sync
  (:require [placeavote_data_sync.bill.bill-parser :as parser]
            [placeavote_data_sync.bill.bill-dynamo-service :as dynamo]
            [clojure.tools.logging :as log]
            [carica.core :refer :all]))

(def bills-dir (config :govtrack-data-home :bills-home))

(defn sync-bills-in-dir [dir]
  (log/info "Syncing Bills in Directory " dir)
  (let [parsed-bills (parser/parse-bills-from-dir dir)]
    (dynamo/save-bills-and-subjects parsed-bills)))


(defn sync-bills-in-dirs [dirs]
  (doseq [dir dirs]
    (log/info "Syncing Bills in Directory " dir)
      (let [parsed-bills (parser/parse-bills-from-dir dir)]
        (dynamo/save-bills-and-subjects parsed-bills))))

(defn sync-bills-in-bills-home []
  (log/info "Starting sync job in " bills-dir)
  (let [hconres (str bills-dir "/hconres")
        hjres (str bills-dir "/hjres")
        hr (str bills-dir "/hr")
        hres (str bills-dir "/hres")
        s (str bills-dir "/s")
        sconres (str bills-dir "/sconres")
        sjres (str bills-dir "/sjres")
        sres (str bills-dir "/sres")]
    (sync-bills-in-dirs [hconres hjres hr hres s sconres sjres sres])))

(defn -main [& args]
  (sync-bills-in-bills-home))

