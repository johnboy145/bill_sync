(ns placeavote_data_sync.sync.gov-sync
  (:require [placeavote_data_sync.bill.bill-sync :as bill-sync]
            [placeavote_data_sync.vote.vote-sync :as vote-sync]
            [placeavote_data_sync.people.people-sync :as people-sync]
            [placeavote_data_sync.bill.bill-dynamo-dao :as bill-dao]
            [placeavote_data_sync.vote.vote-dynamo-dao :as vote-dao]
            [placeavote_data_sync.people.people-dynamo-dao :as people-dao]
            [clojure.tools.logging :as log]))


(defn -main [& args]
  (people-dao/create-people-table)
  (bill-dao/create-bills-table)
  (bill-dao/create-bill-subject-table)
  (vote-dao/create-vote-table)
  (vote-dao/create-voter-vote-table)
  (vote-dao/create-bill-voter-table)
  (vote-dao/create-category-vote-table)
  (people-sync/sync-people)
  (vote-sync/sync-votes-in-votes-home)
  (bill-sync/sync-bills-in-bills-home))
