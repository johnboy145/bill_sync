(ns placeavote_data_sync.api.handler
  (:require [compojure.handler :as handler]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [cheshire.core :as cheshire]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [placeavote_data_sync.bill.bill-dynamo-service :as bill-service]
            [placeavote_data_sync.vote.vote-dynamo-service :as vote-service]
            [placeavote_data_sync.people.people-dynamo-dao :as people-service]
            [placeavote_data_sync.vote.user-vote-dynamo-service :as user-vote-service]))

(defn json-response [status]
  {:status (or status 200)
   :headers {"Content-Type" "application/json" "Access-Control-Allow-Origin" "*"}})

(defn- register-vote [vote]
  (user-vote-service/save-user-vote (:username vote) (:bill vote) (:vote vote))
  (json-response 201))

(defroutes app-routes
  (GET "/bill/:id" [id] (cheshire/generate-string (bill-service/get-bill-by-id id)))
  (GET "/bill/state/:state" [state] (cheshire/generate-string (bill-service/get-bills-by-state state)))
  (GET "/bill/subject/:subject" [subject] (cheshire/generate-string (bill-service/get-bills-by-subject subject)))
  (GET "/bill/type/:ty" [ty] (cheshire/generate-string (bill-service/get-bills-by-type ty)))
  (GET "/bill/sponsor/:sponsor" [sponsor] (cheshire/generate-string (bill-service/get-bills-by-sponsor sponsor)))
  (GET "/bill/vote/:bill" [bill] (cheshire/generate-string (user-vote-service/get-bill-user-vote-by-bill bill)))
  (GET "/vote/:id" [id] (cheshire/generate-string (vote-service/get-vote-by-id id)))
  (GET "/people/:govtrack_id" [govtrack_id] (cheshire/generate-string (people-service/get-person-by-id govtrack_id)))
  (GET "/people/state/:state" [state] (cheshire/generate-string (people-service/get-person-by-state state)))
  (GET "/people/party/:party" [party] (cheshire/generate-string (people-service/get-person-by-party party)))
  (PUT "/user/vote" request  (register-vote (cheshire/parse-string (slurp (:body request)) true)))
  (GET "/user/vote/:user" [user] (cheshire/generate-string (user-vote-service/get-user-vote-by-username user)))
  (GET "/user/vote/:user/:bill" [user bill] (cheshire/generate-string (user-vote-service/get-user-vote-by-username-bill user bill)))
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (handler/site app-routes))
