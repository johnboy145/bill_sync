(ns placeavote_data_sync.people.people-dynamo-dao
  (:require [clojure.tools.logging :as log]
            [placeavote_data_sync.dynamo.dynamo-dao :as dynamo]
            [taoensso.faraday :as far]
            [carica.core :refer :all]))

(def people-table-name "people")

(def client-opts (config :dynamodb-creds))

(defn delete-people-table []
  (far/delete-table client-opts people-table-name))

(defn create-people-table []
  (far/ensure-table client-opts people-table-name [:govtrack_id :s]
  {:throughput {:read 1 :write 1}
   :block? true
   :gsindexes [{:name "state-idx"
               :hash-keydef [:state :s]
               :throughput {:read 2 :write 2}
               :projection :all}
               {:name "party-idx"
               :hash-keydef [:party :s]
               :throughput {:read 2 :write 2}
               :projection :all}]}))

(defn save-people [people]
  (dynamo/batch-write-item client-opts people-table-name people))

(defn get-person-by-id [id]
  (far/get-item client-opts people-table-name {:govtrack_id id}))

(defn get-person-by-state [state]
  (far/query client-opts people-table-name {:state [:eq state]} {:index "state-idx"}))

(defn get-person-by-party [party]
  (far/query client-opts people-table-name {:party [:eq party]} {:index "party-idx"}))
