(ns placeavote_data_sync.people.people-sync
  (:require [placeavote_data_sync.people.people-csv-parser :as parser]
            [placeavote_data_sync.people.people-dynamo-dao :as dynamo]
            [clojure.tools.logging :as log]
            [carica.core :refer :all]))

(def people-csv (config :govtrack-data-home :people-csv))

(defn sync-people []
  (let [parsed-people (parser/parse-csv->map people-csv)]
    (log/info "Syncing People in " people-csv)
    (log/info (str "Syncing " (count parsed-people) " People"))
    (dynamo/save-people parsed-people)))
