(ns placeavote_data_sync.people.people-csv-parser
  (:require [clojure.java.io :as io]
            [clojure.walk :as walk]
            [csv-map.core :as csv]))


(defn- remove-empty [people]
  (into []
    (for [person people]
      (into {} (filter (comp not empty? val) person)))))

(defn parse-csv->map [resource]
  (let [contents (-> resource io/file slurp)]
    (remove-empty (walk/keywordize-keys (csv/parse-csv contents)))))
