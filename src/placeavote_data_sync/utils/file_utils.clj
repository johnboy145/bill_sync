(ns placeavote_data_sync.utils.file-utils
  (:refer-clojure :exclude [name parents])
  (:require [clojure.xml :as xml]
            [clojure.java.io :as io]
            [clojure.zip :as zip]
            [clojure.data.zip.xml :as zip-xml]
            [me.raynes.fs :refer :all]))

(defn retrieve-root-attributes [fileLoc attributes]
  (let [root (-> fileLoc io/resource io/file xml/parse zip/xml-zip)]
    (into {}
          (for [attr attributes]
            [attr (zip-xml/attr root attr)]))))

(defn retrieve-roots-attributes [root attributes]
    (into {}
          (for [attr attributes]
            [attr (zip-xml/attr root attr)])))

(defn retrieve-tag-value [fileLoc tag]
  (let [root (-> fileLoc io/resource io/file xml/parse zip/xml-zip)]
    (zip-xml/xml1-> root
                    tag
                    zip-xml/text)))

(defn retrieve-tags-value [root tag]
    (zip-xml/xml1-> root
                    tag
                    zip-xml/text))

(defn retrieve-tag-attr-value [fileLoc tag attr]
  (let [root (-> fileLoc io/resource io/file xml/parse zip/xml-zip)]
    (zip-xml/xml1-> root
                    tag
                    (zip-xml/attr attr))))

(defn retrieve-tags-attr-value [root tag attr]
    (zip-xml/xml1-> root
                    tag
                    (zip-xml/attr attr)))

(defn list-directories [path]
  (filter #(= (.isDirectory %) true) (.listFiles (io/file path))))

(defn retrieve-file-from-dir [path]
  (io/file path))


