(ns placeavote_data_sync.vote.user-vote-dynamo-dao
  (:require [taoensso.faraday :as far]
            [carica.core :refer :all]
            [clojure.tools.logging :as log]
            [placeavote_data_sync.bill.bill-dynamo-serialiser :refer :all]))

(def user-vote-table "user-vote")
(def bill-user-vote-table "bill-user-votes")
(def client-opts (config :dynamodb-creds))

(defn delete-user-vote-table []
  (far/delete-table client-opts user-vote-table))

(defn create-user-vote-table []
  (far/ensure-table client-opts user-vote-table [:username :s]
  {:throughput {:read 1 :write 1}
   :block? true
   :range-keydef [:bill :s]}))

(defn delete-bill-user-vote-table []
  (far/delete-table client-opts bill-user-vote-table))

(defn create-bill-user-vote-table []
  (far/ensure-table client-opts bill-user-vote-table [:bill :s]
  {:throughput {:read 1 :write 1}
   :block? true}))

(defn save-bill-user-vote [user-vote]
  (far/put-item client-opts bill-user-vote-table user-vote))

(defn increment-bill-vote [bill vote]
  (cond
   (= :aye vote) (far/update-item client-opts bill-user-vote-table {:bill bill} {:aye [:add 1]})
   (= :nay vote) (far/update-item client-opts bill-user-vote-table {:bill bill} {:nay [:add 1]})))

(defn create-bill-user-vote [bill vote]
  (cond
   (= :aye vote) (save-bill-user-vote {:bill bill :aye 1 :nay 0})
   (= :nay vote) (save-bill-user-vote {:bill bill :aye 0 :nay 1})))

(defn save-user-vote [vote]
  (far/put-item client-opts user-vote-table vote))

(defn get-user-vote-by-username-bill [username bill]
  (far/get-item client-opts user-vote-table {:username username :bill bill}))

(defn get-user-vote-by-username [username]
  (far/query client-opts user-vote-table {:username [:eq username]}))

(defn get-bill-user-vote-by-bill [bill]
  (far/get-item client-opts bill-user-vote-table {:bill bill}))

(defn update-bill-user-vote [bill vote]
  (let [persisted-vote (get-bill-user-vote-by-bill bill)]
    (if (empty? persisted-vote)
      (create-bill-user-vote bill vote)
      (increment-bill-vote bill vote))))
