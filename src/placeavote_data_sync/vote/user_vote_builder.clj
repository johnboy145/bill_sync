(ns placeavote_data_sync.vote.user-vote-builder
  (:require [clj-time.core :as t]))

(defn build-user-vote [username vote bill]
  {:username username
   :vote vote
   :bill bill
   :created (.toString (t/now))})
