(ns placeavote_data_sync.vote.vote-dynamo-service
  (:require [placeavote_data_sync.vote.vote-dynamo-dao :as dynamo]
            [placeavote_data_sync.vote.vote-parser :as parser]))

(defn save-category-votes [votes]
  (let [category-votes (vec (map #(parser/parse-category-vote->map %) votes))]
    (dynamo/save-category-votes category-votes)))

(defn save-votes [votes]
  (dynamo/save-votes votes)
  (dynamo/save-voters-votes votes)
  (dynamo/save-bill-voters (filter #(contains? % :bill) votes))
  (save-category-votes votes))

(defn get-vote-by-id [id]
  (dynamo/get-vote-by-id id))

(defn get-category-votes-by-category [category]
  (dynamo/get-category-vote-by-category category))

(defn get-voter-votes-by-id [id]
  (dynamo/get-voter-votes-by-id id))

(defn get-bill-voters-by-bill [bill]
  (dynamo/get-bill-voters-by-id bill))
