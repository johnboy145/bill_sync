(ns placeavote_data_sync.vote.user-vote-dynamo-service
  (:require [placeavote_data_sync.vote.user-vote-dynamo-dao :as dao]
            [placeavote_data_sync.vote.user-vote-builder :as builder]))

(defn save-user-vote [username bill vote]
  (dao/save-user-vote (builder/build-user-vote username vote bill))
  (dao/update-bill-user-vote bill (keyword vote)))

(defn get-user-vote-by-username [username]
  (dao/get-user-vote-by-username username))

(defn get-user-vote-by-username-bill [username bill]
  (dao/get-user-vote-by-username-bill username bill))

(defn get-bill-user-vote-by-bill [bill]
  (dao/get-bill-user-vote-by-bill bill))
