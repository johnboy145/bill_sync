(ns placeavote_data_sync.vote.vote-sync
  (:require [placeavote_data_sync.vote.vote-parser :as parser]
            [placeavote_data_sync.vote.vote-dynamo-service :as dynamo]
            [clojure.tools.logging :as log]
            [carica.core :refer :all]))

(def votes-dir (config :govtrack-data-home :votes-home))

(defn sync-votes-in-dir [dir]
  (log/info "Syncing Votes in Directory " dir)
  (let [parsed-votes (parser/parse-votes-from-dir dir)]
    (dynamo/save-votes parsed-votes)))


(defn sync-votes-in-dirs [dirs]
  (doseq [dir dirs]
    (log/info "Syncing Votes in Directory " dir)
      (let [parsed-votes (parser/parse-votes-from-dir dir)]
        (dynamo/save-votes parsed-votes))))

(defn sync-votes-in-votes-home []
  (log/info "Starting sync job in " votes-dir)
    (let [dir1 (str votes-dir "/2013")
          dir2 (str votes-dir "/2014")]
      (sync-votes-in-dirs [dir1 dir2])))

(defn -main [& args]
  (sync-votes-in-votes-home))
