(ns placeavote_data_sync.vote.vote-parser
  (:require [placeavote_data_sync.utils.file-utils :refer :all]
            [clojure.xml :as xml]
            [clojure.java.io :as io]
            [clojure.zip :as zip]
            [clojure.data.zip.xml :as zip-xml]))

(defn parse-bill-id [root]
  (let [bill (zip-xml/xml1-> root :bill)]
    (if-not (empty? bill)
      {:bill (str (zip-xml/attr bill :type)
                  (zip-xml/attr bill :number))}
      nil)))

(defn parse-vote-id [root]
  (str (nth (zip-xml/attr root :where) 0) (zip-xml/attr root :roll) "-" (zip-xml/attr root :year)))

(defn parse-voters [root]
  (into []
        (into []
              (for [voter (zip-xml/xml-> root :voter)]
                (-> {:id (zip-xml/attr voter :id)}
                    (conj {:value (zip-xml/attr voter :value)})
                    (conj {:state (zip-xml/attr voter :state)})
                    (conj (parse-bill-id root))
                    (conj {:vote (parse-vote-id root)}))))))

(defn parse-bill [root]
  (let [bill (zip-xml/xml1-> root :bill)]
    (if-not (empty? bill)
      {:bill (->
              {:session (zip-xml/attr bill :session)}
              (conj {:id (str (zip-xml/attr bill :type)
                              (zip-xml/attr bill :number))}))}
      nil)))

(defn parse-ammendment [root]
  (let [amendment (zip-xml/xml1-> root :amendment)]
    (if-not (empty? amendment)
      {:amendment (->
                   {:ref (zip-xml/attr amendment :ref)}
                   (conj {:number (zip-xml/attr amendment :number)})
                   (conj {:session (zip-xml/attr amendment :session)}))}
      nil)))

(defn parse-vote->map [resource]
  (let [root (-> resource io/file xml/parse zip/xml-zip)]
    (->
     (retrieve-roots-attributes root [:where :session
                                     :year :roll :source
                                     :datetime :updated
                                     :aye :nay :nv :present])
     (conj {:id (parse-vote-id root)})
     (conj {:category (retrieve-tags-value root :category)})
     (conj {:type (retrieve-tags-value root :type)})
     (conj {:question (retrieve-tags-value root :question)})
     (conj {:required (retrieve-tags-value root :required)})
     (conj {:result (retrieve-tags-value root :result)})
     (conj {:voters (parse-voters root)})
     (conj (parse-bill root))
     (conj (parse-ammendment root)))))

(defn parse-category-vote->map [vote]
      (-> {:category (:category vote)}
          (conj {:id (:id vote)})
          (conj {:question (:question vote)})))

(defn parse-votes-from-dir [dir]
  (let [voteDirs (list-directories dir)]
    (into []
      (for [dir voteDirs]
        (parse-vote->map (str (.getPath dir) "/data.xml"))))))
