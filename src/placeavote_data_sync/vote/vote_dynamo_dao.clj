(ns placeavote_data_sync.vote.vote-dynamo-dao
  (:require [taoensso.faraday :as far]
            [placeavote_data_sync.dynamo.dynamo-dao :as dynamo]
            [placeavote_data_sync.bill.bill-dynamo-serialiser :refer :all]
            [carica.core :refer :all]
            [clojure.tools.logging :as log]))

(def client-opts (config :dynamodb-creds))

(def vote-table-name "vote")
(def vote-category-table-name "votecategory")
(def voter-vote-table-name "voter-vote")
(def bill-voter-table-name "bill-voter")

(defn delete-vote-table []
  (far/delete-table client-opts vote-table-name))

(defn delete-category-vote-table []
  (far/delete-table client-opts vote-category-table-name))

(defn delete-voter-vote-table []
  (far/delete-table client-opts voter-vote-table-name))

(defn delete-bill-voter-table []
  (far/delete-table client-opts bill-voter-table-name))

(defn create-vote-table []
  (far/ensure-table client-opts vote-table-name [:id :s]
  {:throughput {:read 1 :write 1}
   :block? true
   :gsindexes [{:name "chamber-idx"
               :hash-keydef [:where :s]
               :range-keydef [:updated :s]
               :throughput {:read 2 :write 2}
               :projection :all}]}))

(defn create-category-vote-table []
  (far/ensure-table client-opts vote-category-table-name [:category :s]
  {:throughput {:read 1 :write 1}
   :block? true
   :range-keydef [:id :s]}))

(defn create-voter-vote-table []
  (far/ensure-table client-opts voter-vote-table-name [:id :s]
  {:throughput {:read 1 :write 1}
   :block? true
   :range-keydef [:vote :s]}))

(defn create-bill-voter-table []
  (far/ensure-table client-opts bill-voter-table-name [:bill :s]
  {:throughput {:read 1 :write 1}
   :block? true
   :range-keydef [:id :s]}))

(defn save-vote [vote]
  "Persist Vote to DynamoDB"
  (far/put-item client-opts vote-table-name (serialise-bill-map vote)))

(defn save-votes [votes]
  "Persist Votes to DynamoDB"
  (log/info (count votes) " votes saved to dynamo")
  (let [serialised-votes (serialise-bills votes)]
    (dynamo/batch-write-item client-opts vote-table-name serialised-votes)))

(defn save-category-vote [vote]
  "Persist Vote to DynamoDB"
  (far/put-item client-opts vote-category-table-name (serialise-bill-map vote)))

(defn save-voter-votes [votes]
    "Persist Vote to DynamoDB"
  (log/info (count votes) " voter votes saved to dynamo")
  (let [serialised-votes (serialise-bills (:voters votes))]
    (dynamo/batch-write-item client-opts voter-vote-table-name serialised-votes)))

(defn save-voters-votes [voters-votes]
    "Persist Vote to DynamoDB"
  (log/info (count voters-votes) " voters votes saved to dynamo")
  (doseq [voter-vote voters-votes]
    (let [serialised-votes (serialise-bills (:voters voter-vote))]
    (dynamo/batch-write-item client-opts voter-vote-table-name serialised-votes))))

(defn save-bill-voters [votes]
    "Persist Voters related to a bill to DynamoDB"
  (log/info (count votes) " bill voters saved to dynamo")
  (doseq [voter-vote votes]
    (let [serialised-votes (serialise-bills (:voters voter-vote))]
     (dynamo/batch-write-item client-opts bill-voter-table-name serialised-votes))))

(defn save-category-votes [votes]
  (log/info (count votes) " category votes saved to dynamo")
  (let [serialised-votes (serialise-bills votes)]
    (dynamo/batch-write-item client-opts vote-category-table-name serialised-votes)))

(defn get-vote-by-id [id]
  (deserialise-bill-map (far/get-item client-opts vote-table-name {:id id})))

(defn get-vote-by-chamber [chamber]
  (deserialise-bills-map (far/query client-opts vote-table-name {:where [:eq chamber]} {:index "chamber-idx"})))

(defn get-category-vote-by-category [category]
  (deserialise-bills-map (far/query client-opts vote-category-table-name {:category [:eq category]})))

(defn get-voter-votes-by-id [voter]
  (deserialise-bills-map (far/query client-opts voter-vote-table-name {:id [:eq voter]})))

(defn get-bill-voters-by-id [bill]
  (deserialise-bills-map (far/query client-opts bill-voter-table-name {:bill [:eq bill]})))

